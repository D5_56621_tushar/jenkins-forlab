create table user (
    firstname varchar(50),
    lastname varchar(50),
    email varchar(50) primary key,
    password varchar(1000),
    mobno varchar(50),
    dob date,
    gender varchar(50)
);

CREATE TABLE Product (
    Id INTEGER PRIMARY KEY auto_increment,
    Title VARCHAR(100),
    Description VARCHAR(500),
    Price FLOAT,
    CategoryId INTEGER
);

CREATE TABLE Category (
    Id INTEGER PRIMARY KEY auto_increment,
    Title VARCHAR(100),
    Description VARCHAR(500)
);

INSERT INTO Category values (default, 'Electronics', 'Electronic Products');
INSERT INTO Category values (default, 'Computers', 'Electronic computers');
INSERT INTO Category values (default, 'TV', 'Telivisions');


INSERT INTO Product values (default, 'man', 'man', 45, 1);